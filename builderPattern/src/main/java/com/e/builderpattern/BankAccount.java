package com.e.builderpattern;

class BankAccount {
    private long accountNumber;
    private String owner;
    private double balance;
    private double rate;

    private BankAccount(){}

    public static class Builder{
        private long accountNumber;
        private String owner;
        private double balance;
        private double rate;

        public Builder(long accountNumber){
            this.accountNumber=accountNumber;
        }

        public Builder setOwner(String owner){
            this.owner=owner;
            return this;
        }

        public Builder setBalance(double balance){
            this.balance=balance;
            return this;
        }

        public Builder setRate(double rate){
            this.rate=rate;
            return this;
        }

        public BankAccount build(){
            BankAccount bankAccount = new BankAccount();
            bankAccount.accountNumber=this.accountNumber;
            bankAccount.owner=this.owner;
            bankAccount.balance=this.balance;
            bankAccount.rate=this.rate;
            return bankAccount;
        }
    }
}
