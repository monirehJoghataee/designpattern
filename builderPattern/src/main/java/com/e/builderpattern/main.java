package com.e.builderpattern;

class main {
    public static void main(){
        BankAccount bankAccount = new BankAccount.Builder(12329L)
                .setOwner("monireh")
                .setBalance(1000000)
                .setRate(5.0)
                .build();
    }
}
