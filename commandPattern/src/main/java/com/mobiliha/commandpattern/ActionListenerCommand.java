package com.mobiliha.commandpattern;

interface ActionListenerCommand {
    public void execute();
}
