package com.mobiliha.commandpattern;

class ActionOpen implements ActionListenerCommand {
    Document doc;

    public ActionOpen(Document doc) {
        this.doc = doc;
    }

    @Override
    public void execute() {
        doc.open();
    }
}
