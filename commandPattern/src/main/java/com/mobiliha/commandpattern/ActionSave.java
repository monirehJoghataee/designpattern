package com.mobiliha.commandpattern;

class ActionSave implements ActionListenerCommand{
    Document doc;

    public ActionSave(Document doc) {
        this.doc = doc;
    }

    @Override
    public void execute() {
        doc.save();
    }
}
