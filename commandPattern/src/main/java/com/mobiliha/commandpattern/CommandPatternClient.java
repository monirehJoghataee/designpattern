package com.mobiliha.commandpattern;

class CommandPatternClient {
    public static void main(String[] args) {
        Document doc = new Document();
        ActionListenerCommand open = new ActionOpen(doc);
        ActionListenerCommand save = new ActionSave(doc);

        MenuOptions menuOptions = new MenuOptions(open, save);
        menuOptions.clickOpen();
        menuOptions.clickSave();
    }
}
