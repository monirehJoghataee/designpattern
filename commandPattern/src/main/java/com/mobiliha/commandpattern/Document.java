package com.mobiliha.commandpattern;

public class Document {
    public void open(){
        System.out.println("document is opened");
    }

    public void save(){
        System.out.println("document is seved");
    }
}
