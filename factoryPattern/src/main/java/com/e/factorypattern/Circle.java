package com.e.factorypattern;

import android.widget.Toast;

class Circle implements Shape{
    @Override
    public void draw() {
        System.out.println("circle");
    }
}
