package com.e.factorypattern;

interface Shape {
    void draw();
}
