package com.e.factorypattern;

class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("square");
    }
}
