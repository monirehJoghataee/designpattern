package com.e.iteratorpattern;

interface Container {
    public Iterator getIterator();
}
