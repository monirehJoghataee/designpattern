package com.e.iteratorpattern;

interface Iterator {
    public boolean hasNext();
    public Object next();
}
