package com.e.iteratorpattern;

class IteratorPatternDemo {
    public static void main(){
        NameRepository nameRepository = new NameRepository();
        for (Iterator iterator = nameRepository.getIterator() ; iterator.hasNext();){
            String name = (String) iterator.next();
            System.out.println("Name: " + name);
        }
    }
}
