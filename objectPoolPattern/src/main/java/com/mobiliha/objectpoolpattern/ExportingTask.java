package com.mobiliha.objectpoolpattern;

class ExportingTask implements Runnable{
    private ObjectPool<ExportingProcess> mPool;
    private int threadNo;
    public ExportingTask(ObjectPool<ExportingProcess> pool,int threadNo){
        this.mPool=pool;
        this.threadNo=threadNo;
    }

    @Override
    public void run() {
        // get an object from the pool
        ExportingProcess exportingProcess = mPool.borrowObject();
        System.out.println("Thread " + threadNo + ": Object with process no. "
                + exportingProcess.getProcessNo() + " was borrowed");

        //you can  do something here in future
        // .........


        // return ExportingProcess instance back to the pool
        mPool.returnObject(exportingProcess);

    }
}
