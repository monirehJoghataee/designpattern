package com.mobiliha.objectpoolpattern;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class ObjectPool<T> {
    private ConcurrentLinkedQueue<T> pool;
    private ScheduledExecutorService mExecutorService;

    public ObjectPool(final int minObject) {
        initialize(minObject);
    }

    public ObjectPool(final int minObject, final int maxObject, final long validationInterval) {
        initialize(minObject);

        mExecutorService = Executors.newSingleThreadScheduledExecutor();
        mExecutorService.scheduleWithFixedDelay((Runnable) () -> {
            int size = pool.size();
            if (size < minObject) {
                int sizeToBeAdded = minObject + size;
                for (int i = 0; i < sizeToBeAdded; i++) {
                    pool.add(createObject());
                }
            } else if (size > maxObject) {
                int sizeToBeRemoved = size - maxObject;
                for (int i = 0; i < sizeToBeRemoved; i++) {
                    pool.poll();
                }
            }
        }, validationInterval, validationInterval, TimeUnit.SECONDS);
    }

    public T borrowObject() {
        T object;
        if ((object = pool.poll()) == null) {
            object = createObject();
        }
        return object;
    }

    public void returnObject(T object) {
        if (object == null) {
            return;
        }
        pool.offer(object);
    }

    public void shutdown() {
        if (mExecutorService != null) {
            mExecutorService.shutdown();
        }
    }

    protected abstract T createObject();

    private void initialize(int minObject) {
        pool = new ConcurrentLinkedQueue<T>();
        for (int i=0;i<minObject;i++){
            pool.add(createObject());
        }
    }
}
