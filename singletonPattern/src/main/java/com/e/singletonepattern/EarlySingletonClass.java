package com.e.singletonepattern;

class EarlySingletonClass {
    private static final EarlySingletonClass ourInstance = new EarlySingletonClass();

    static EarlySingletonClass getInstance() {
        return ourInstance;
    }

    private EarlySingletonClass() {
    }
}
