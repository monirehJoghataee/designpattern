package com.e.singletonepattern;

class LazySingletonClass {
    private static LazySingletonClass ourInstance ;

    static LazySingletonClass getInstance() {
        if(ourInstance==null){
            ourInstance=new LazySingletonClass();
        }
        return ourInstance;
    }

    private LazySingletonClass() {
    }
}
