package com.e.singletonepattern;

class Main {

    public static void main(String[] args) {
        EarlySingletonClass earlySingletonClass = EarlySingletonClass.getInstance();
        LazySingletonClass lazySingletonClass = LazySingletonClass.getInstance();
    }
}
