package com.mobiliha.strategypattern;

public interface PaymentStrategy {

    public void pay(int amount);

}
