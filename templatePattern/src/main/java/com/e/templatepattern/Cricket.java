package com.e.templatepattern;

class Cricket extends Game{

    @Override
    protected void startGame() {
        System.out.println("Cricket Game Started. Enjoy the game!");
    }

    @Override
    protected void play() {
        System.out.println("Cricket Game Initialized! Start playing.");
    }

    @Override
    protected void endGame() {
        System.out.println("Cricket Game Finished!");
    }
}
