package com.e.templatepattern;

import android.util.Log;

class FootballPlay extends Game {


    @Override
    protected void startGame() {
        System.out.println("Football Game Started. Enjoy the game!");
    }

    @Override
    protected void play() {
        System.out.println("Football Game Initialized! Start playing.");
    }

    @Override
    protected void endGame() {
        System.out.println("Football Game Finished!");
    }

}
