package com.e.templatepattern;

public abstract class Game {
    protected abstract void startGame();
    protected abstract void play();
    protected abstract void endGame();

    public void playGame(){
        startGame();
        play();
        endGame();
    }

}
