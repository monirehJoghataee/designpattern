package com.e.templatepattern;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Game footballPlay=new FootballPlay();
        footballPlay.playGame();
        Game cricket=new Cricket();
        cricket.playGame();
    }
}